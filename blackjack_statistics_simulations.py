from blackjack import *
from util import *
from statistics import mean, stdev

def sim_1v1(number_of_rounds_of_games, number_of_games, number_of_decks, bet=Game.BetEnum.OFF):
    winnings_dict_list = []
    for round_number in range(number_of_rounds_of_games):
        game_dict_list = individual_game_simulation(
            number_of_games=number_of_games,
            number_of_decks=number_of_decks,
            print_result=False,
            bet=bet)
        winnings_dict_list.append(game_dict_list)

    dealer_winning_frequencies = [
        player_tuple[1]['Winnings']/player_tuple[1]['Number_of_Hands']
        for game_dict_list in winnings_dict_list
        for player_tuple in game_dict_list
        if player_tuple[0] == 'Dealer'
    ]
    print('AVG freq of Dealer Winnings:  {}'.format(
        mean(dealer_winning_frequencies)))
    print('SD of Dealer Winnings:  {}'.format(
        stdev(dealer_winning_frequencies)))

    tie_frequencies = [
        player_tuple[1]['Ties']/player_tuple[1]['Total_Number_of_Hands']
        for game_dict_list in winnings_dict_list
        for player_tuple in game_dict_list
        if player_tuple[0] == 'Pushes'
    ]
    print('AVG freq of Ties:  {}'.format(mean(tie_frequencies)))
    print('SD of Ties:  {}'.format(stdev(tie_frequencies)))

    # avg_player_winnings = mean(
    #     [round_dict['Player 0'][0] for round_dict in winnings_dict])
    # avg_player_hands = mean(
    #     [round_dict['Player 0'][1] for round_dict in winnings_dict])

    player_winning_frequencies = [
        player_tuple[1]['Winnings']/player_tuple[1]['Player_Hands']
        for game_dict_list in winnings_dict_list
        for player_tuple in game_dict_list
        if player_tuple[0] == 'Player 0'
        ]

    print('AVG freq of Player Winnings:  {}'.format(
        mean(player_winning_frequencies)))
    print('SD freq of Player Winnings:  {}'.format(
        stdev(player_winning_frequencies)))

    player_DD_frequencies = [
        player_tuple[1]['Double_Down_Winnings']/player_tuple[1]['Player_Hands']
        for game_dict_list in winnings_dict_list
        for player_tuple in game_dict_list
        if player_tuple[0] == 'Player 0'
    ]
    print('AVG freq of Double Down Winnings:  {}'.format(
        mean(player_DD_frequencies)))
    print('SD freq of Player Winnings:  {}'.format(
        stdev(player_DD_frequencies)))

    player_blackjack_frequencies = [
        player_tuple[1]['Blackjack_Winnings']/player_tuple[1]['Player_Hands']
        for game_dict_list in winnings_dict_list
        for player_tuple in game_dict_list
        if player_tuple[0] == 'Player 0'
    ]

    print('AVG freq of Blackjack Winnings:  {}'.format(
        mean(player_blackjack_frequencies)))
    print('SD freq of Blackjack Winnings:  {}'.format(
        stdev(player_blackjack_frequencies)))

    player_reeval_freqs = [
        player_tuple[1]['Net_Winnings'] / player_tuple[1]['Player_Hands']
        for game_dict_list in winnings_dict_list
        for player_tuple in game_dict_list
        if player_tuple[0] == 'Player 0'
    ]
    print('Adjusted Player Winnings:  {}'.format(mean(player_reeval_freqs)))

    player_net_winnings_freqs = [
        player_tuple[1]['Net_Winnings']
        for game_dict_list in winnings_dict_list
        for player_tuple in game_dict_list
        if player_tuple[0] == 'Player 0'
    ]
    print('Net Winnings:  {}'.format(sum(player_net_winnings_freqs)))
    # print('SD Net Winnings:  {}'.format(stdev(player_net_winnings_freqs)))


    player_net_earnings_freqs = [
        player_tuple[1]['Net_Earnings']
        for game_dict_list in winnings_dict_list
        for player_tuple in game_dict_list
        if player_tuple[0] == 'Player 0'
    ]
    print('Net Earnings:  {}'.format(sum(player_net_earnings_freqs)))
    print(player_net_earnings_freqs)
    # print('SD Net Earnings:  {}'.format(stdev(player_net_earnings_freqs)))