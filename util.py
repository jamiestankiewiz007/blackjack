from enum import Enum

class SplitEnum(Enum):
    SPLIT = 'split'
    DONT_SPLIT = "don't split"

class MoveEnum(Enum):
    HIT = 'hit'
    STAY = 'stay'
    DOUBLE_DOWN = 'double down'
    SURRENDER = 'surrender'

class Card(object):
    class SuitEnum(Enum):
        HEARTS = 'hearts'
        DIAMONDS = 'diamonds'
        SPADES = 'spades'
        CLUBS = 'clubs'

    ranks = [str(number) for number in range(2,11)] + \
            ['jack', 'queen', 'king', 'ace']
    rank_value = {}
    for rank in ranks:
        try:
            rank_value[rank] = int(rank)
        except:
            if rank == 'ace':
                rank_value[rank] = (1,11)
            else:
                rank_value[rank] = 10

    def __init__(self, rank, suit):
        self.rank = rank
        self.suit = suit
        self.face = 'down'

    def __str__(self):
        return '{} of {}'.format(self.rank, self.suit)


class Hand(object):
    def __init__(self, card_set):
        self.cards = card_set # list of Card objects
        assert len(self.cards) == 2 # initially
        self.winner = None
        self.double_down = False

    @property
    def ace_in_hand(self):
        return 'ace' in [card.rank for card in self.cards]

    @property
    def pair_in_hand(self):
        return len(set([card.rank for card in self.cards])) == 1

    @property
    def bust(self):
        return self.sum > 21
        #return sum(Card.rank_value[card.rank] for card in self.cards) > 21
    
    @property
    def blackjack(self):
        return self.sum == 21
    

    @property
    def sum(self):
        if 'ace' in [card.rank for card in self.cards]:
            num_aces = len([card for card in self.cards if card.rank == 'ace'])
            without_ace = [card for card in self.cards if card.rank != 'ace']
            without_ace_sum = sum(
                Card.rank_value[card.rank] for card in without_ace)
            if num_aces == 1:
                lower_sum = 1 + without_ace_sum
                higher_sum = 11 + without_ace_sum
                if higher_sum > 21:
                    return lower_sum
                else:
                    return higher_sum
            else:
                assert num_aces > 1
                higher_sum = 11 + (num_aces - 1) + without_ace_sum
                lower_sum = num_aces + without_ace_sum
                if higher_sum > 21:
                    return lower_sum
                else:
                    return higher_sum
        else:
            return sum([Card.rank_value[card.rank] for card in self.cards])

    @property
    def count(self):
        high_cards = ('10', 'jack', 'queen', 'king', 'ace')
        neutral_cards = ('7', '8', '9')
        low_cards = ('2', '3', '4', '5', '6')
        hand_count = 0
        for card in self.cards:
            if card.rank in low_cards:
                hand_count += 1
            elif card.rank in high_cards:
                hand_count -= 1
        return hand_count    


class Player(object):
    def __init__(self, hand):
        self.hand = hand
        assert hand.cards
        assert len(hand.cards) == 2
        self.hands = [self.hand]
    

def split_dealt_cards(dealt_cards):
    '''
    dealt_cards is a list of the total number of cards at the table.
    split_dealt_cards divides the cards up into hands for each player.
    The output should be a list of tuples, where each tuples contains a pair
    of card objects.
    '''
    assert type(dealt_cards) == list
    number_of_players = int(len(dealt_cards)/2)
    hands = [[dealt_cards.pop(), dealt_cards.pop()]
                for i in range(number_of_players)]
    return hands

def determine_to_split_pair(hand, dealers_upcard):
    '''
    If a player's hand contains 2 of the same ranked cards, this logic will
    return if the player should split based on the dealer's upcard.
    '''
    assert len(hand.cards) == 2
    card_1 = hand.cards[0]
    card_2 = hand.cards[1]
    assert card_1.rank == card_2.rank
    dealers_upcard = Card.rank_value[dealers_upcard.rank]
    players_card = Card.rank_value[card_1.rank]
    if card_1.rank == 'ace' or players_card == 8:
        return SplitEnum.SPLIT
    if type(dealers_upcard) == tuple:
        if card_1.rank in ('ace', '8'):
            return SplitEnum.SPLIT
        else:
            return SplitEnum.DONT_SPLIT
    if players_card in (5, 10):
        return SplitEnum.DONT_SPLIT
    if players_card in (2, 3, 7):
        if dealers_upcard <= 7:
            return SplitEnum.SPLIT
        else:
            return SplitEnum.DONT_SPLIT
    if players_card == 6:
        if dealers_upcard <= 6:
            return SplitEnum.SPLIT
        else:
            return SplitEnum.DONT_SPLIT
    if players_card == 4:
        if dealers_upcard in (5, 6):
            return SplitEnum.SPLIT
        else:
            return SplitEnum.DONT_SPLIT
    if players_card == 9:
        if dealers_upcard in (7, 10, (1, 11)):
            return SplitEnum.DONT_SPLIT
        else:
            return SplitEnum.SPLIT
    else:
        print('Pair Splitting Error {}'.format(players))
        class PairSplittingError(Exception):
            pass


def hand_result(hand, dealers_upcard):
    '''
    Determine whether to HIT, STAY, or DOUBLE_DOWN given a hand, and a dealer's
    upcard.
    '''
    dealers_upcard_value = Card.rank_value[dealers_upcard.rank]
    if type(dealers_upcard_value) == tuple:
        dealers_upcard_value = 11
    if hand.ace_in_hand: # soft-total
        no_ace_card_sum = sum([
            Card.rank_value[card.rank]
            for card in hand.cards if card.rank != 'ace'])
        if no_ace_card_sum < 10:
            hand_total = hand.sum

            if hand_total >= 20:
                return MoveEnum.STAY
            elif hand_total == 19:
                return MoveEnum.DOUBLE_DOWN if dealers_upcard_value == 6\
                    else MoveEnum.STAY
            elif hand_total == 18:
                if dealers_upcard_value <= 6:
                    return MoveEnum.DOUBLE_DOWN
                elif dealers_upcard_value in (7,8):
                    return MoveEnum.STAY
                else:
                    return MoveEnum.HIT
            elif hand_total == 17:
                return MoveEnum.DOUBLE_DOWN if dealers_upcard_value in range(3,7)\
                    else MoveEnum.HIT
            elif hand_total in (15,16):
                return MoveEnum.DOUBLE_DOWN if dealers_upcard_value in range(4,7)\
                    else MoveEnum.HIT
            elif hand_total in (13,14):
                return MoveEnum.DOUBLE_DOWN if dealers_upcard_value in range(5,7)\
                    else MoveEnum.HIT
            else:
                # print('Soft Total Error: ', hand.sum)
                class SoftTotalError(Exception):
                    pass
    if hand.sum >= 17:
        return MoveEnum.STAY
    elif hand.sum >= 13:
        return MoveEnum.STAY if dealers_upcard_value < 6\
            else MoveEnum.HIT
    elif hand.sum == 12:
        return MoveEnum.STAY if dealers_upcard_value in range(4,7)\
            else MoveEnum.HIT
    elif hand.sum == 11:
        return MoveEnum.DOUBLE_DOWN
    elif hand.sum == 10:
        return MoveEnum.DOUBLE_DOWN if dealers_upcard_value <= 9\
            else MoveEnum.HIT
    elif hand.sum == 9:
        return MoveEnum.DOUBLE_DOWN if dealers_upcard_value in range(3,7)\
            else MoveEnum.HIT
    else:
        return MoveEnum.HIT
