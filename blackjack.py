from enum import Enum
from random import sample, shuffle
from functools import reduce
from util import *

cards = [Card(rank=rank, suit=suit)
            for suit in [enum.value for enum in Card.SuitEnum]
            for rank in Card.ranks
    ]

class Game(object):
    class GamePlayEnum(Enum):
        INDIVIDUAL = '1v1'
        TABLE = 'table game'

    class BetEnum(Enum):
        ON = 'on'
        OFF = 'off'

    def __init__(self, cards, number_of_decks,
            game_play=GamePlayEnum.INDIVIDUAL, number_of_players=1,
            bet=BetEnum.OFF):
        self.cards = cards
        self.number_of_decks = number_of_decks
        self.deck = reduce(
            lambda x, y: x + y, \
            [sample(cards, len(cards)) for i in range(self.number_of_decks)])
        shuffle(self.deck)
        self.game_play = game_play
        self.number_of_players = number_of_players
        self.bet = bet
        if self.game_play == Game.GamePlayEnum.TABLE:
            assert self.number_of_players != 1
        else:
            assert self.game_play == Game.GamePlayEnum.INDIVIDUAL
            assert self.number_of_players == 1
        self.running_count_list = []

    @property
    def size_of_bet(self):
        '''
        Your next betting unit should be the true count minus 1.
        Measured in units of bet.
        '''
        if self.running_count_list:
            return self.true_count-1
        else:
            assert not self.running_count_list
            return 1

    @property
    def running_count(self):
        return sum(self.running_count_list)

    @property
    def true_count(self):
        approx_decks_remaining = round((len(self.deck)/52) * 2) / 2
        if approx_decks_remaining == 0:
            approx_decks_remaining = .5
        if self.running_count_list:
            return self.running_count/approx_decks_remaining
        else:
            return 0
    
    def reshuffle_deck(self):
        self.deck = reduce(
            lambda x, y: x + y, \
            [sample(self.cards, len(self.cards))
            for i in range(self.number_of_decks)])
        shuffle(self.deck)

    @property
    def show_deck(self):
        return [str(card) for card in self.deck]

    def run_game(self):
        '''
        Run a game with (self.number_of_players) number of players against a 
        dealer, with (self.number_of_decks) of decks.
        Determine how many winners there are.
        '''
        if self.game_play == Game.GamePlayEnum.INDIVIDUAL:
            assert self.number_of_players == 1
        else:
            assert self.game_play == Game.GamePlayEnum.TABLE
            assert self.number_of_players != 1
        try:
            dealt_cards = [
                self.deck.pop() for i in range((self.number_of_players + 1)*2)]
        except:
            self.reshuffle_deck()
            dealt_cards = [
                self.deck.pop() for i in range((self.number_of_players + 1)*2)]
        # +1 for the dealer
        table_hands = split_dealt_cards(dealt_cards)
        # initialize dealer Player object
        dealer = Player(Hand(table_hands.pop()))
        assert len(dealer.hand.cards) == 2
        dealers_upcard = dealer.hand.cards[0]
        # initialize Player objects
        players = []
        
        for hand in table_hands:
            # -1 b/c dealer
            players.append(Player(Hand(hand)))
        
        ties = 0
        
        if dealer.hand.sum == 21:
            dealer.hand.winner = True
            for player in players:
                if player.hand.sum != 21:
                    player.hand.winner = False
                else:
                    assert player.hand.sum == 21
                    ties += 1
                    player.hand.winner = None
            dealer_winnings = len([True for player in players
                if player.hand.winner == False])
            number_of_dealer_hands = len(players) - ties
            number_of_player_hands = len(players)
        else:
            # INITIAL BET
            # players decide their initial move
            for player in players:
                assert len(player.hand.cards) == 2
                assert len(player.hands) == 1
                for player_hand in player.hands:
                    # is player hand a double
                    if player_hand.pair_in_hand:
                        # determine if player should split their hand
                        split_result = determine_to_split_pair(
                            player_hand, dealers_upcard)
                        if split_result == SplitEnum.SPLIT:
                            assert len(player_hand.cards) == 2
                            card_1 = player_hand.cards[0]
                            card_2 = player_hand.cards[1]
                            try: # if the deck of cards run out
                                player_hand = Hand([card_1, self.deck.pop()])
                                player.hands.append(
                                    Hand([card_2, self.deck.pop()]))
                            except:
                                self.reshuffle_deck()
                                player_hand = Hand([card_1, self.deck.pop()])
                                player.hands.append(
                                    Hand([card_2, self.deck.pop()]))

                    while hand_result(
                            player_hand, dealers_upcard) != MoveEnum.STAY\
                            and not player_hand.bust:
                        move = hand_result(player_hand, dealers_upcard)
                        if move == MoveEnum.DOUBLE_DOWN:
                            # the player hits once and that is the end of their 
                            # game
                            player_hand.double_down = True
                            try: # if the deck of cards run out
                                player_hand.cards.append(self.deck.pop())
                            except:
                                self.reshuffle_deck()
                                player_hand.cards.append(self.deck.pop())
                            break
                        if move == MoveEnum.HIT:
                            try:
                                player_hand.cards.append(self.deck.pop())
                            except:
                                self.reshuffle_deck()
                                player_hand.cards.append(self.deck.pop())
                    if player_hand.bust:
                        player_hand.winner = False

            # dealer flips over their other card, and makes a decision to hit
            player_totals = [
                hand.sum for player in players for hand in player.hands]
            # print(player_totals)
            alive_player_totals = [
                hand.sum for player in players for hand in player.hands
                if hand.sum < 21]
            if alive_player_totals:
                while (dealer.hand.sum < 17 and not all([
                        dealer.hand.sum >= player_total for player_total
                        in alive_player_totals]))\
                        and not dealer.hand.bust:
                    #dealer will hit
                    try:
                        dealer.hand.cards.append(self.deck.pop())
                    except:
                        self.reshuffle_deck()
                        dealer.hand.cards.append(self.deck.pop())
                    
            # determine who wins
            # dealer gets blackjack
            if dealer.hand.sum == 21:
                dealer.hand.winner = True
                for player in players:
                    for player_hand in player.hands:
                        if player_hand.sum == 21:
                            ties += 1
                            player_hand.winner = None
                        else:
                            player_hand.winner = False
                dealer_winnings = len([True for player in players
                    for player_hand in player.hands
                    if player_hand.winner == False
                    ])
            # dealer busts
            elif dealer.hand.bust:
                dealer.hand.winner = False
                for player in players:
                    for player_hand in player.hands:
                        if not player_hand.bust:
                            player_hand.winner = True
                        else:
                            player_hand.winner = False
                dealer_winnings = 0
            # neither player nor dealer busts
            # determine who has a higher score
            else:
                for player in players:
                    for player_hand in player.hands:
                        if not player_hand.bust and\
                                (player_hand.sum > dealer.hand.sum or\
                                player_hand.sum == 21):
                            # player hand wins
                            player_hand.winner = True
                        elif player_hand.sum == dealer.hand.sum:
                            ties += 1
                            player_hand.winner = None
                        else:
                            # didn't bust, but didn't win
                            # or bust
                            assert (player_hand.bust or\
                                player_hand.sum < dealer.hand.sum)
                            player_hand.winner = False

                dealer_winnings = len([True for player in players
                    for player_hand in player.hands
                    if player_hand.winner == False
                    ])

            number_of_dealer_hands = len(player_totals) - ties
            number_of_player_hands = len(player_totals)
        
        # return an output list of dicts once the game is over
        output_dict_list = [
            ('Dealer',
                {'Winnings': dealer_winnings,
                'Number_of_Hands': number_of_dealer_hands}),
            ('Pushes',
                {'Ties': ties,'Total_Number_of_Hands': number_of_player_hands})]

        for player in players:
            player_winnings = len([player_hand for player_hand in player.hands
                if player_hand.winner])
            player_pushes = len([player_hand for player_hand in player.hands
                if player_hand.winner == None])
            player_double_down_wins = len([player_hand for player_hand
                in player.hands if player_hand.double_down == True
                and player_hand.winner])
            player_blackjack_wins = len([player_hand for player_hand
                in player.hands if player_hand.blackjack])
            player_losses = len([player_hand for player_hand in player.hands
                if not player_hand.winner])
            player_double_down_losses = len([player_hand for player_hand in player.hands
                if ((not player_hand.winner) and player_hand.double_down)])
            # return the number of times the player won, as well as the number
            # of hands the player had.
            output_dict_list.append(
                ('Player {}'.format(players.index(player)),
                    {'Winnings': player_winnings,
                    'Double_Down_Winnings': player_double_down_wins,
                    'Blackjack_Winnings': player_blackjack_wins,
                    'Player_Hands': len(player.hands) - player_pushes,
                    'Double_Down_Losings': player_double_down_losses,
                    'Losses': player_losses}
                )
            )
            
        # print(output_dict)
        running_count = sum([
            hand.count for hand in player.hands for player in players])
        self.running_count_list.append(running_count)
        return output_dict_list
        # Note: the sum of the 3 numbers will always be equal or lower to the
        # sum of number of hands. The missing tallies reflect the case where
        # both the player's hand AND the dealer bust.
    
    def run_game_with_bet(self):
        '''
        Run the game, but now add weighted values to the winnings of each player
        '''
        assert self.bet == Game.BetEnum.ON
        #assert self.size_of_bets > 0
        #weight = self.size_of_bets
        size_of_bet = self.size_of_bet
        if size_of_bet < 1:
            size_of_bet = 1
        bet_outcome_list = self.run_game()

        for player_tuple in bet_outcome_list:
            if 'Player' in player_tuple[0]:
                player_tuple[1]['Net_Winnings'] =\
                    (player_tuple[1]['Winnings'] +\
                     player_tuple[1]['Double_Down_Winnings'] +\
                     .5*player_tuple[1]['Blackjack_Winnings'] +\
                     -player_tuple[1]['Losses'] +\
                     -player_tuple[1]['Double_Down_Losings'])
                player_tuple[1]['Net_Earnings'] =\
                    (size_of_bet)*(player_tuple[1]['Winnings'] +\
                    (size_of_bet)*player_tuple[1]['Double_Down_Winnings'] +\
                    (size_of_bet/2)*player_tuple[1]['Blackjack_Winnings']) + \
                    (-size_of_bet)*player_tuple[1]['Losses'] +\
                    (-size_of_bet)*player_tuple[1]['Double_Down_Losings']
        return bet_outcome_list


def run_simulation(game_play, number_of_players, number_of_games,
        number_of_decks, print_result = True, bet=Game.BetEnum.OFF):
    if game_play == Game.GamePlayEnum.TABLE:
        g = Game(
            cards=cards,
            game_play=game_play,
            number_of_decks=number_of_decks,
            number_of_players=number_of_players,
            bet=bet)
    elif game_play == Game.GamePlayEnum.INDIVIDUAL:
        g = Game(
            cards=cards,
            game_play=game_play,
            number_of_decks=number_of_decks,
            number_of_players=1,
            bet=bet)
    else:
        class GamePlayError(Exception):
            print('Game Play Error')

    summary_totals = [
        ('Dealer', {'Winnings': 0, 'Number_of_Hands': 0}),
        ('Pushes', {'Ties': 0, 'Total_Number_of_Hands': 0})
    ]
    for p in range(number_of_players):
        summary_totals.append(
            ('Player {}'.format(p),
                {'Winnings': 0,
                'Double_Down_Winnings': 0,
                'Blackjack_Winnings': 0,
                'Player_Hands': 0,
                'Net_Winnings': 0,
                'Net_Earnings': 0})
        )
    for i in range(number_of_games):
        if g.bet == Game.BetEnum.OFF:
            output_dict_list = g.run_game()
        else:
            assert g.bet == Game.BetEnum.ON
            output_dict_list = g.run_game_with_bet()
        for output_tuple in output_dict_list:
            player_id = output_tuple[0]
            output_dict = output_tuple[1]
            if player_id == 'Dealer':
                summary_totals[0][1]['Winnings'] += output_dict['Winnings']
                summary_totals[0][1]['Number_of_Hands'] += \
                    output_dict['Number_of_Hands']
            elif player_id == 'Pushes':
                summary_totals[1][1]['Ties'] += output_dict['Ties']
                summary_totals[1][1]['Total_Number_of_Hands'] +=\
                    output_dict['Total_Number_of_Hands']
            else:
                #TODO: fix the indexing?
                assert 'Player' in player_id
                index = [summary_totals.index(player_dict) for
                    player_dict in summary_totals
                    if player_dict[0] == player_id]
                assert len(index) == 1
                summary_totals[index[0]][1]['Winnings'] +=\
                    output_dict['Winnings']
                summary_totals[index[0]][1]['Double_Down_Winnings'] +=\
                    output_dict['Double_Down_Winnings']
                summary_totals[index[0]][1]['Blackjack_Winnings'] +=\
                    output_dict['Blackjack_Winnings']
                summary_totals[index[0]][1]['Player_Hands'] +=\
                    output_dict['Player_Hands']
                if g.bet == Game.BetEnum.ON:
                    summary_totals[index[0]][1]['Net_Winnings'] +=\
                        output_dict['Net_Winnings']
                    summary_totals[index[0]][1]['Net_Earnings'] +=\
                        output_dict['Net_Earnings']

    if print_result == True:
        for player_tuple in summary_totals:
            player_id = player_tuple[0]
            player_dict = player_tuple[1]
            if player_id == 'Dealer':
                print(player_id, ': {}/{}'.format(
                    player_dict['Winnings'], player_dict['Number_of_Hands']))
            elif player_id == 'Pushes':
                print(player_id, ': {}/{}'.format(
                    player_dict['Ties'], player_dict['Total_Number_of_Hands']))
            else:
                print(
                    '{}: Wins: ({}/{}, DD_Wins: {}/{}, Blackjacks: {}/{})'
                    .format(
                        player_id,
                        player_dict['Winnings'], player_dict['Player_Hands'],
                        player_dict['Double_Down_Winnings'],
                        player_dict['Player_Hands'],
                        player_dict['Blackjack_Winnings'],
                        player_dict['Player_Hands']
                        )
                )
                if g.bet == Game.BetEnum.ON:
                    print('Player {} Net Winnings: {}'.format(
                        player_id, player_dict['Net_Winnings']))
                    print('Player {} Net Earnings: {}'.format(
                        player_id, player_dict['Net_Earnings']))

    else:
        return summary_totals


def run_table_simulation(
        number_of_players, number_of_games, number_of_decks, print_result=True,
        bet=Game.BetEnum.OFF):
    '''
    Run a multiple playered game n times and print the output statistics of 
    the dealer and each player.
    '''
    return run_simulation(
        game_play=Game.GamePlayEnum.TABLE,
        number_of_players=number_of_players,
        number_of_games=number_of_games,
        number_of_decks=number_of_decks,
        print_result=print_result,
        bet=bet)

def individual_game_simulation(
        number_of_games, number_of_decks, print_result=True,
        bet=Game.BetEnum.OFF):
    '''
    Run a single player game n times, with (number_of_decks) number of decks
    and print the output statistics of the dealer and player.
    '''
    return run_simulation(
        game_play=Game.GamePlayEnum.INDIVIDUAL,
        number_of_players=1,
        number_of_games=number_of_games,
        number_of_decks=number_of_decks,
        print_result=print_result,
        bet=bet)